class ButtonBlock extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initButtonBlock()
    }

    initButtonBlock () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: ButtonBlock")
    }

}

window.customElements.define('fir-button-block', ButtonBlock, { extends: 'div' })
