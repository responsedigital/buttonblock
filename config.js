module.exports = {
    'name'  : 'ButtonBlock',
    'camel' : 'ButtonBlock',
    'slug'  : 'button-block',
    'dob'   : 'N/A',
    'desc'  : 'A row of buttons (up to 3)',
}