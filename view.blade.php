<!-- Start ButtonBlock -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A row of buttons (up to 3) -->
@endif
<div class="button-block" is="fir-button-block" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="button-block__wrap">
    <a href="" class="btn btn--blue">
    Link One
    </a>
    <a href="" class="btn btn--blue">
    Link Two
    </a>
    <a href="" class="btn btn--blue">
    Link Three
    </a>
  </div>
</div>
<!-- End ButtonBlock -->