<?php

namespace Fir\Pinecones\ButtonBlock;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'ButtonBlock',
            'label' => 'Pinecone: Button Block',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A row of buttons (up to 3)"
                ],
                [
                    'label' => 'Links',
                    'name' => 'linksTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => 'Links',
                    'name' => 'link',
                    'type' => 'repeater',
                    'layout' => 'table',
                    'min' => 1,
                    'max' => 3,
                    'button_label' => 'Add Link',
                    'sub_fields' => [
                        [
                            'label' => 'Link',
                            'name' => 'link',
                            'type' => 'link'
                        ],
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
